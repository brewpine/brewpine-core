--
-- ──────────────────────────────────────────────────────────────────────────────────── I ──────────
--   :::::: S V A L T E K   C O M M O N   M E T H O D S : :  :   :    :     :        :          :
-- ──────────────────────────────────────────────────────────────────────────────────────────────
--
-- Collection of Usefull common Methods
-- ────────────────────────────────────────────────────────────────────────────────

-- @function compose
---* Create a function composition from given functions.
-- any further functions as arguments get added to composition in order
--- @param f1 function
-- the outermost function of the composition
--- @param f2 function
-- second outermost function of the composition
--- @return function the composite function
local function compose(f1, f2, ...)
    if select('#', ...) > 0 then
        local part = compose(f2, ...)
        return compose(f1, part)
    else
        return function(...)
            return f1(f2(...))
        end
    end
end

-- @function bind
---* Create a function with bound arguments ,
-- The bound function returned will call func() ,
-- with the arguments passed on to its creation .
-- If more arguments are given during its call, they are ,
-- appended to the original ones .
-- `...` the arguments to bind to the function.
--- @param func function
-- the function to create a binding of
--- @return function
-- the bound function
local function bind(func, ...)
    local saved_args = {...}
    return function(...)
        local args = {table.unpack(saved_args)}
        for _, arg in ipairs({...}) do
            table.insert(args, arg)
        end
        return func(table.unpack(args))
    end
end

-- @function bind_self
---* Create f bound function whose first argument is t ,
--  Particularly useful to pass a method as a function ,
-- Equivalent to bind(t[k], t, ...) ,
-- `...` further arguments to bind to the function.
--- @param t table Binding
-- The table to be accessed
--- @param k any Key
-- The key to be accessed
--- @return function BoundFunc
-- The binding for t[k]
local function bind_self(t, k, ...)
    local selfbind = bind
    return selfbind(t[k], t, ...)
end

--- @section Getters and Setters

---* Create a function that returns the value of t[k] ,
-- The returned function is Bound to the Provided Table,Key.
--- @param t table
--table to access
--- @param k any
--key to return
--- @return function
-- returned getter function
local function bind_getter(t, k)
    return function()
        if (not type(t) == 'table') then
            return nil, 'Bound object is not a table'
        elseif (t == {}) then
            return nil, 'Bound table is Empty'
        elseif (t[k] == nil) then
            return nil, 'Bound Key does not Exist'
        else
            return t[k], 'Fetched Bound Key'
        end
    end
end

---* Create a function that sets the value of t[k] ,
--- The returned function is Bound to the Provided Table,Key ,
--- The argument passed to the returned function is used as the value to set.
--- @param t table       table to access
--- @param k table       key to set
--- @return function     returned setter function
local function bind_setter(t, k)
    return function(v)
        if (not type(t) == 'table') then
            return nil, 'Bound object is not a table'
        elseif (t == {}) then
            return nil, 'Bound table is Empty'
        elseif (t[k] == nil) then
            return nil, 'Bound Key does not Exist'
        else
            t[k] = v
            return true, 'Set Bound Key'
        end
    end
end

---* Create a function that returns the value of t[k] ,
--- The argument passed to the returned function is used as the Key.
--- @param t table       table to access
--- @return function     returned getter function
local function getter(t)
    if (not type(t) == 'table') then
        return nil, 'Bound object is not a table'
    elseif (t == {}) then
        return nil, 'Bound table is Empty'
    else
        return function(k)
            return t[k]
        end
    end
end

---* Create a function that sets the value of t[k] ,
--- The argument passed to the returned function is used as the Key.
--- @param t table       table to access
--- @return function     returned setter function
local function setter(t)
    if (not type(t) == 'table') then
        return nil, 'Bound object is not a table'
    elseif (t == {}) then
        return nil, 'Bound table is Empty'
    else
        return function(k, v)
            t[k] = v
            return true
        end
    end
end

-- @section File Management

---* Check if a file or directory exists in path ,
--- Path is Relative to Server Root.
---@param path string path - the path to a file or directory to test
local function Exists(path)
    local ok, err, code = os.rename(path, path)
    if not ok then
        if code == 13 then
            -- Permission denied, but it exists
            return true
        end
    end
    return ok, err
end

local function isFile(path)
    local f = io.open(path, 'r')
    if f then
        f:close()
        return true
    end
    return false
end
-- Check if a directory exists path
local function isDir(path)
    path = string.gsub(path .. '/', '//', '/')
    local ok, err, code = os.rename(path, path)
    if ok or code == 13 then
        return true
    end
    return false
end

local function mkDir(path)
    local ok, Result = os.execute('mkdir ' .. path:gsub('/', '\\'))
    if not ok then
        return nil, 'Failed to Create ' .. path .. ' Directory! - ' .. Result
    else
        return true, 'Successfully Created ' .. path .. ' Directory!'
    end
end

---* Write file to Disk
--- @param path string       path of file to Write, starts in Server root
--- @param data any          File Contents to Write
--- @return boolean,string   true,nil and a message
local function WriteFile(path, data)
    local thisFile = assert(io.open(path, 'w'))
    if thisFile ~= nil then
        local fWritten = thisFile:write(data)
        thisFile:close()
        if fWritten ~= nil then
            return true, 'Success Writing File: <ServerRoot>/' .. path
        else
            return nil, 'Failed to Write Data to File: <ServerRoot>/' .. path
        end
    else
        return nil, 'Failed to Open file for Writing: <ServerRoot>/' .. path
    end
end

---* Read File from Disk
--- @param path string      path of file to Write, starts in Server root
--- @return boolean,any     true,nil and file content or message
local function ReadFile(path)
    local thisFile, errMsg = io.open(path, 'r')
    if thisFile ~= nil then
        local fContent = thisFile:read('*all')
        thisFile:close()
        if fContent ~= '' or nil then
            return true, fContent
        else
            return nil, 'Failed to Read from File: ' .. path
        end
    else
        return nil, 'Error Opening file: ' .. path .. ' io.open returned:' .. errMsg
    end
end

--- @section ChatCommand Utils

local function cmdSplit(pString, pPattern)
    local Table = {}
    local fpat = '(.-)' .. pPattern
    local last_end = 1
    local s, e, cap = pString:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= '' then
            table.insert(Table, cap)
        end
        last_end = e + 1
        s, e, cap = pString:find(fpat, last_end)
    end
    if last_end <= #pString then
        cap = pString:sub(last_end)
        table.insert(Table, cap)
    end
    return Table
end

local function parseArgs(command)
    local cmdLine = cmdSplit(command, ' ')
    local cmdChunks = {}
    local ix = 0
    for iChunk, cmdChunk in pairs(cmdLine) do
        if ix ~= 1 then
            cmdChunks['0'] = cmdChunk
            ix = 1
        else
            local aKey, aValue = cmdChunk:match('([^,]+)=([^,]+)')
            if aValue ~= nil then
                cmdChunks[aKey] = aValue
            end
        end
    end
    return cmdChunks
end

-- @section Table Functions

---* Pretty Print (Dumps) Tables/Objects/Strings with formatting
--- @param value any
-- value to Pretty Print
local function pretty(value, level)
    local level = level or 0
    local output = {}
    local insert, concat, format = table.insert, table.concat, string.format

    local function add(line, indent)
        insert(output, format('%s%s', string.rep('  ', indent or 0), line))
    end
    local function isarray(t)
        if type(t) ~= 'table' then
            return false
        end
        for k, _ in pairs(t) do
            if type(k) ~= 'number' then
                return false
            end
        end
        return true
    end
    local function pretty_array(t)
        local strings = {}
        for _, v in ipairs(t) do
            insert(strings, pretty(v, level + 1))
        end
        return concat(strings, ', ')
    end

    if type(value) == 'table' then
        if isarray(value) then
            return format('[ %s ]', pretty_array(value))
        else
            local keys = {}
            for k, _ in pairs(value) do
                if type(k) ~= 'number' then
                    insert(keys, k)
                end
            end
            table.sort(keys)
            add('{')
            for _, k in ipairs(keys) do
                add(format('%s = %s', k, pretty(value[k], level + 1)), level + 1)
            end
            for _, v in ipairs(value) do
                add(pretty(v, level + 1), level + 1)
            end
            add('}', level)
        end
    elseif type(value) == 'string' then
        add(format("'%s'", value))
    else
        add(format('%s', tostring(value)))
    end
    return concat(output, '\n')
end

---* Return the Size of a Table.
-- Works with non Indexed Tables
--- @param table table  any table to get the size of
--- @return number      size of the table
local function table_size(table)
    local n = 0
    for k, v in pairs(table) do
        n = n + 1
    end
    return n
end

---* Copies all the fields from the source into t and return .
-- If a key exists in multiple tables the right-most table value is used.
--- @param t table      table to update
local function table_update(t, ...)
    for i = 1, select('#', ...) do
        local x = select(i, ...)
        if x then
            for k, v in pairs(x) do
                t[k] = v
            end
        end
    end
    return t
end


return {
    -- Functional Programming Helpers
    compose = compose,
    bind = bind,
    bind_self = bind_self,
    -- Getters and Setters
    getter = getter,
    setter = setter,
    bind_getter = bind_getter,
    bind_setter = bind_setter,
    -- File Management
    exists = Exists,
    isFile = isFile,
    isDir = isDir,
    mkDir = mkDir,
    ReadFile = ReadFile,
    WriteFile = WriteFile,
    -- String Methods
    string_cmdSplit = cmdSplit,
    string_parseArgs = parseArgs,
    -- Table Methods
    table_pretty = pretty,
    table_size = table_size,
    table_update = table_update,
}