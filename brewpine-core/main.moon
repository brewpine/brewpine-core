-- Debugging
if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1" then
    require("lldebugger").start()

-- ──────────────────────────────────────────────────────── I ──────────
--   :::::: B R E W P I N E : :  :   :    :     :        :          :
-- ──────────────────────────────────────────────────────────────────
--

require "lfs"

local BP_ENV
BP_ENV =
    Version:    "1.0a"
    BaseDir:    (arg[1] or "/usr/BrewPine")
    Updater:
        BaseUrl: "https://brewpine.gitlab.io/brewpine-updates/"


path_seperator = package.config\sub(1,1)

extend_path = (base_path) ->
    local path
    path = base_path .. "#{path_seperator}"
    package.path = "#{path}init.lua;#{path}?.lua;#{path}?.so;#{package.path}"

extend_path "#{BP_ENV['BaseDir']}"

-- Imports
local utils
utils = require "brewpine-core.modules.common"

import string_cmdSplit from utils

-- Main Program

-- localise program args
local args
args = _G['arg']

-- Fetch and parse Command Args
local cmd_args
cmd_args = string_cmdSplit "#{args[2]}", '::'

print "     .: Welcome to BrewPine v#{BP_ENV['Version']} :."
print " Args: "
for i,argv in pairs cmd_args
    print i,argv